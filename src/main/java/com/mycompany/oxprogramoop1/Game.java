

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author NITRO 5
 */
package com.mycompany.oxprogramoop1;
import java.util.Scanner;
public class Game {

    Scanner kb = new Scanner(System.in);
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row;
    int col;
    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }
    public void showWelcome() {
        System.out.println("Welcome OX Game");
    }
    public void showTable() {
        table.showTable();
    }
    public void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Eror : table at row and col is not emty!");
        }
    }
    public void showTurn() {
        System.out.println(table.getcurrentPlayer().getName() + " turn");
    }
    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    showTable();
                    System.out.println("Draw!!");
                    System.out.println("Bye bye ...");  
                } else {
                    showTable();
                    System.out.println("Player "+table.getWinner().getName() + " Win");
                    System.out.println("Bye bye ...");
                } 
                break;
            }
            table.switchPlayer();
        }
    }
}
